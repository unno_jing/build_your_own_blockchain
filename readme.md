# Build you own blockchain

## How to run

```sh
uvicorn main:app
```

# Reference

- [udemy course](https://www.udemy.com/course/build-blockchain/learn/lecture/9314302#overview)

- [blog tutorial](https://hackernoon.com/learn-blockchains-by-building-one-117428612f46)
- [source code](https://github.com/dvf/blockchain/blob/master/blockchain.py)
